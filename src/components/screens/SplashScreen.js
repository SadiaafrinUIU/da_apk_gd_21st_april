import React, {Component } from 'react';
import {
  StyleSheet,
  View,
  // Image,
  Text,
  ToastAndroid,
  TextInput,
  ActivityIndicator,
  TouchableOpacity,
  NetInfo,
  Dimensions,
  Platform,
  Image,
  AsyncStorage,
  // StatusBar,
} from 'react-native';
import ExtraDimensions from 'react-native-extra-dimensions-android';
// import OfflineNoticeLogin from '../views/OfflineNoticeLogin';
// import Snackbar from 'react-native-snackbar';
import { NavigationActions, StackActions } from 'react-navigation';
import PropTypes from 'prop-types';
// import LoginButton from './../buttons/LoginButton';
import * as actions from './../../stores/actions/index';
import {
  Container,
  Content,
  Form,
} from 'native-base';

import { connect } from 'react-redux';
import { authAction } from './../../stores/actions/authAction';


// let localCredentialValue =null;
class SplashScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {
      username: null,
      password: null,
      loading: false,
      connectionStatus: true,
    };
  }


  componentDidMount() {

    this.checkUserSignedIn();

  }

  static navigationOptions = {
    header: null,
    drawerLockMode: 'locked-closed',
  };


  async checkUserSignedIn() {
    const context = this;
    try {
      const localCredentialValue = await AsyncStorage.getItem('user');

      if (localCredentialValue != null) {

        const forwardLocalStorage_to_redux = JSON.parse(localCredentialValue);



        this.props.onAuth(forwardLocalStorage_to_redux.name, forwardLocalStorage_to_redux.password);


        // this caused an error difficult to find.


        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            // NavigationActions.navigate({ routeName: 'LoginScreen' }),
             NavigationActions.navigate({ routeName: 'OrderList' }),  // replace the 2nd Screen here.

            // for test

           // NavigationActions.navigate({ routeName: 'OrderedItems'}),  // replace the 2nd Screen here.


          ],
        });

        this.props.navigation.dispatch(resetAction);


        // this.props.navigation.navigate('OrderedItems');

        // setTimeout(() => {
        //   this.props.navigation.dispatch(navigateAction);
        // }, 200);

      }
      else {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'LoginScreen' }),
            // NavigationActions.navigate({ routeName: 'OrderedItems' }),
            // replace the 2nd Screen here.
          ],
        });

        this.props.navigation.dispatch(resetAction);



        // setTimeout(() => {
        //   this.props.navigation.dispatch(resetAction);
        // }, 500);
      }

      // Login Screen Redirection
    } catch (error) {
    }
  }


  render() {
    const deviceWidth = Dimensions.get('window').width;
    const deviceHeight = Platform.OS === 'ios'
      ? Dimensions.get('window').height
      : ExtraDimensions.get('REAL_WINDOW_HEIGHT');

    const deleveryImage = require('./../../assets/delivery-truck.png');
    // require('./../../assets/icons/delivery-truck.png')
    return (
      <View style={styles.container}>
        <Image
          source={deleveryImage}
          // source={require('./../../assets/icons/delivery-truck.png')}
          // source={{ uri: 'https://www.esri.com/content/dam/esrisites/industries/government/overview-hero-illustration.png' }}
          // delivery-truck.png
          style={styles.image}/>
        <Text style={styles.text}>Delivery Application </Text>

        <View style={{
          position: 'absolute',
          bottom: 200,
          marginLeft: deviceWidth * 0.5 - 20,
        }}>

          <ActivityIndicator size='large' color='#FF6969'/>
        </View>
      </View>
    );
  }
}



    const styles = StyleSheet.create({
      container: {
        alignItems: 'center',
        flex: 3,
        justifyContent: 'center',
      },
      image: {
        height: 80,
        width: 80,

      },
      text: {
        backgroundColor: 'transparent',
        color: '#FF6969',
        fontWeight: 'bold',
        marginTop: 20,
      },
    });



SplashScreen.propTypes = {
  navigation: PropTypes.object,
  onAuth: PropTypes.func.isRequired,
  // loading:PropTypes.bool,
  // error:PropTypes.object,
  navigate:PropTypes.object,
  // token: PropTypes.string,


};
SplashScreen.defaultProps = {

};


// Not neccessary in this page.
// thus null in --> here -->  export default connect(null,
// const mapStateToProps = state => {
//
//   return{
//     loading:state.auth.loading,
//     error: state.auth.error,
//     username: state.auth.username,
//     token: state.auth.token,
//   };
// };

// export default

const  mapDispatchToProps = dispatch  =>
  ({
    // hits actions.authAction
    onAuth:  (username,password) => dispatch(actions.authAction(username,password)),
    //clearAuthStore:() => dispatch(actions.clearAuthStore()),
    // clearAuthStore:( username, password,loading) => dispatch(actions.clearAuthStore(username, password,loading)),
    //onLogout: () => dispatch(actions.LogoutFromApplication()),
    // listData: state.orderList,
  })
;



export default connect(null,
  mapDispatchToProps
)(SplashScreen );
