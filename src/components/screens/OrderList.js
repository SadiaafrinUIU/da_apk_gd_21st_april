import React, { Component } from 'react';
import { StyleSheet, Text, View, BackHandler, StatusBar, AsyncStorage } from 'react-native';
import OrderListBody from './../views/OrderListBody';
import OrderListHeader from './../views/OrderListHeader';
import { connect } from 'react-redux';
import { getOrderList, clearAuthStore } from './../../stores/actions';

class OrderListPage extends Component {
  constructor(props) {
    super(props);
    //static data to populate order list
    this.state = {
      order: [
        {
          id: '1',
          region: {
            latitude: 62.838880,
            longitude: 27.643540,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          },
          destination: 'Pyörönkaari 24, 70820 Kuopio, Finland',
          distance: '5km',
          timeLimit: 90,
          deliverydate: 'JAN 10',
          deliveryTime: '11 PM',
        },
        {
          id: '2',
          region: {
            latitude: 23.783502,
            longitude: 90.421782,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          },
          destination: 'Kenakata Store, Gudaraghat',
          distance: '5km',
          timeLimit: 10,
          deliverydate: 'JAN 10',
          deliveryTime: '11 PM',
        },
        {
          id: '3',
          region: {
            latitude: 23.822350,
            longitude: 90.365417,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          },
          destination: 'Mirpur 11, Dhaka,1215',
          distance: '5km',
          timeLimit: 90,
          deliverydate: 'JAN 10',
          deliveryTime: '11 PM',
        },
        {
          id: '4',
          region: {
            latitude: 23.822350,
            longitude: 90.365417,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          },
          destination: 'Mirpur 11,Dhaka,1215',
          distance: '5km',
          timeLimit: 90,
          deliverydate: 'JAN 10',
          deliveryTime: '11 PM',
        },
      ],
    };
    //function to redirect to driver navigation page
    this.redirect = this.redirect.bind(this);
    //function to logout the driver
    this.logout = this.logout.bind(this);
    //function to set timer for each order
    this.checkAllTimer = this.checkAllTimer.bind(this);
  }

  componentDidMount() {
    //fetch order list data from backend using a demno api url
    //this.props.getOrderList();
    //set timer seperately for each order
    var timer = setInterval(() => (
      this.checkAllTimer(timer)
    ), 60000);

  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  // componentWillUnmount() {
  //   BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  // }

  handleBackButtonClick = () => {
    if(this.props.navigation.dangerouslyGetParent().state.routes.length == 1) {
      BackHandler.exitApp();
      true;
    } else if(this.props.navigation.dangerouslyGetParent().state.routes.length == 2 && this.props.navigation.dangerouslyGetParent().state.routes[0].routeName == 'LoginScreen') {
      BackHandler.exitApp();
      true;
    }
  }

  checkAllTimer(timer) {
    //check if all the timer set to 0
    const found = this.state.order.find((element) => element.timeLimit > 0);
    //if all timer value is not 0 then countdown continues
    if(found) {
      this.setState(state => {
        const order = state.order.map((item, j) => {
          if (item.timeLimit > 0) {
            item.timeLimit -= 1;
          }
          return item;
        });

        return {
          order,
        };
      });
    } else {
      //clear timer if all timer value is 0
      clearInterval(timer);
    }
  }

  redirect(order) {
    //redirect to driver navigation page
    this.props.navigation.navigate('Target', {
      orderValue: order,
    });
  }


  logout = async () => {
    //clear logout credential from async storage
    try {
      const localCredentialValue = await AsyncStorage.getItem('user');
      if (localCredentialValue != null) {
        try {
          await AsyncStorage.removeItem('user', (err, result) => {
            //after clearing async storage redirect to splash screen
              if (err) {
                return;
              }
              if (result) {
                this.props.clearAuthStore();
                this.props.navigation.navigate('SplashScreen');
                return result;
              }
            this.props.clearAuthStore();
            this.props.navigation.navigate('SplashScreen');
            },
          );
        } catch (error) {
        }

      }
    }
    catch (error) {
      // Error saving data
      // console.log('Error of Try while getting value during logout: ', error);
    }
  };

  render() {
    return (
      <View style={listStyle.orderListContainer}>
        {/*setting status bar color according to page background color*/}
        <StatusBar backgroundColor={'#F7F7FA'} barStyle="dark-content"/>
        <View style={listStyle.listheaderFlex}>
          {/* Order list page header component. props: logoutEvent*/}
          <OrderListHeader logoutEvent={this.logout}></OrderListHeader>
        </View>
        <View style={listStyle.listbodyFlex}>
          {/* order list body containing all the list items. props: redirect event, order list*/}
          <OrderListBody pressEvent={this.redirect} orderList={this.state.order}></OrderListBody>
        </View>
      </View>
    );
  }
}

const listStyle = StyleSheet.create({
  listbodyFlex: {
    flex: .9,
  },
  listheaderFlex: {
    flex: .1,
    justifyContent: 'center',
    marginBottom: 5,
  },
  orderListContainer: {
    backgroundColor: '#F7F7FA',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
});

const mapStateToProps = state => 
  //maps state of redux to component props
   ({
    listData: state.orderList.orderListData,
  })
;

//make a connection between action, reducer and component
export default connect(
  mapStateToProps, {getOrderList, clearAuthStore}
)(OrderListPage);