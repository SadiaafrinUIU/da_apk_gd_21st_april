import {
  ORDER_LIST,
  ORDER_DETAIL,
  LOGIN,
} from '../actions/types';


const INITIAL_STATE = {
  orderListData: [],
}

export default function listReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ORDER_LIST:
      return {...state, orderListData : action.payload};
    // case ORDER_DETAIL:
    //   return state.filter(post => post._id !== action.payload.id);
    // case LOGIN:
    //   return action.posts;
    default:
      return state;
  }
}