import * as types from './types';
// const queryString = require('query-string');
// import qs from 'qs';

import axios from 'axios';


export const authStart = () => ({
    type:types.AUTH_START,
  });

export const authSuccess = (responseToken,initialUsername) => 
  // authData
  // type:types.AUTH_SUCCESS,
  // authData,

  ({
    type:types.AUTH_SUCCESS,
    token:responseToken,
    username:initialUsername,

  })
;

export const authFail = (generatedError) => ({
    type: types.AUTH_FAIL,
    error:generatedError,
  });

export const logout = () => ({
    type: types.AUTH_LOGOUT,
  });


export const reinitiateAuthStore = () => ({
    type: types.CLEAR_TO_INITIAL,
  });



export const clearAuthStore =()=>
   dispatch => {
    dispatch(reinitiateAuthStore());
  }
  // dispatch(authStart());
  // dispatch(authFail(error));
;

export const LogoutFromApplication =()=>dispatch => {
    dispatch(logout());
  };



// From LoginScreen's this line onAuth:  (username,password) => dispatch(actions.authAction(username,password)),'
export const authAction =(username, password) => {

  axios.defaults.baseURL = 'http://18.223.117.205:8080/';

  axios.defaults.headers.post['Content-Type'] = 'application/json';

  // axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';


  // axios.defaults.headers.common['Authorization'] = AUTH_TOKEN;

  // Send a POST request

  // let data:
  // {
  //   "username":"admin",
  //   "password":"admin",
  //   "rememberMe":true
  // };

  //Get bearer
  return dispatch => {
    dispatch(authStart());

    const authdata = {
      username,
      password,
      'rememberMe': true,
    };


    axios({
      method: 'POST',
      url: 'api/authenticate',
      data: authdata,
    }).then((response) => {

      if (response.data.id_token) {


        dispatch(authSuccess(response.data.id_token, username));

        //bearer found.

      }


      // to be tested again
    }).catch((error) => {
      // handle error
      dispatch(authFail(error));

    }).then(() => {
      // always executed
    });

  };
};



// axios.get('api/authenticate/').then(function (response) {
//   // handle success
// })


