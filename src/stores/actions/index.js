import {
  ORDER_LIST,
  ORDER_DETAIL,
  LOGIN,
} from './types';
import urls from '../url';
import axios from 'axios';
import NetworkClient from '../networkClient';

const header = 'Bearer ' + urls.token;


const fetchList = (dispatch, orders) => {
  dispatch({
    type: ORDER_LIST,
    payload: orders,
  });
};

export const getOrderList = () => (dispatch) => NetworkClient.get(urls.orderListUrl)
      .then(response => fetchList(dispatch, response.data))
      .catch(error => {
        throw(error);
      });

export const getOrderDetails = (driverId) => ({
    type: ORDER_LIST,
    payload: driverId,
  });

// export const loginDriver = (loginPayload) => ({
//     type: ORDER_LIST,
//     payload: loginPayload,
//   });
  
  export const loginDriver = (loginCredentials) => ({
    type: ORDER_LIST,
    payload: loginCredentials,
  });

export {
  authAction,
  clearAuthStore,
  LogoutFromApplication,
}from './authAction';