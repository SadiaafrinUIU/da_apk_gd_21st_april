export const ORDER_LIST = 'get_order_list';
export const ORDER_DETAIL = 'get_order_detail';
export const LOGIN = 'login';

export const AUTH_START = 'AUTH_START';
export const AUTH_SUCCESS = 'AUTH_SUCCESS';
export const AUTH_FAIL = 'AUTH_FAIL ';
export const AUTH_LOGOUT = 'AUTH_LOGOUT';
// TEST
export const CLEAR_TO_INITIAL = 'CLEAR_TO_INITIAL';



